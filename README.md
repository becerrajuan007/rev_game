## Rev

**Game Document – Juan Becerra**

**Revision #2 (5/29/2019)**

### Basic Story

  - **oo** A strange dungeon has sprung up in the town of Jacinto. This has attracted the attention of warriors called &quot;Revenants&quot; to go in, explore its depths, slay enemies, and acquire powerful weapons and armor.
  - **oo** On the 100th floor of the dungeon lies the Balrog, the dungeon&#39;s master. Every floor is harder than the last, so Revenants will have to make sure that they&#39;re well-equipped.
  - **oo** Revenants can die and return to the surface, but their gear will be dropped on that floor. Revenants will have to strategically choose which gear to take with them and which gear to keep on the surface in case they die.
-
### Gameplay

  - **oo**** Overview**
    - This is a 3rd-person action-adventure role-playing game.
  - **oo**** Mechanics**
    - **Rules**
      - You start in the hub-world, Jacinto, which is where your character respawns upon death. Here, you can rest at the inn to recover your health, store equipment that you&#39;ve found, and fuse weaker loot to create more powerful loot.
      - Entering the dungeon allows you to enter a randomly-generated &quot;maze&quot; dungeon with randomly generated loot and enemies. Reaching the end progresses to the next maze.
      - Every 5thfloor, you will fight a boss known as a &quot;Knight&quot;.
      - On the 100th floor, you will fight the final boss known as the &quot;Balrog&quot;.
    - **Combat**
      - Dark Souls-like: Heavy emphasis on dodging/blocking telegraphed attacks and countering with your own attacks.
      - Loot can be picked up. They have rarities and levels associated with them and have damage/defense values to reflect that.
      - Enemies are also scaled based off of the dungeon floor and have health/damage values that reflect that.
  - **oo**** Controls**
    - **Mouse and Keyboard**
      - WASD: Movement
      - Mouse: Camera
      - Space: Dodge
      - E: Loot/Interact
      - Left-click: Primary attack
      - Right-click: Secondary attack
      - Shift: Sprint
      - TAB: Lock-on target
      - F: Block

**X-Input Gamepad**

-
  -
    -
      - Left-stick: Movement
      - Right-stick: Camera
      - A: Dodge
      - B: Loot/Interact
      - X: Primary attack
      - Y: Secondary attack
      - Left-stick-click: Sprint
      - Right-stick-click: Lock-on target
      - LB or RB: Block
-
### Space

  - **oo**** The Grave**
    - Underground desert-themed area. This serves as a linear tutorial zone for new players. This area can be skipped (for experienced players).
  - **oo**** Jacinto**
    - The fantasy-themed hub-world. Includes an inn to rest at (recovers health), a bank (to store items), a blacksmith shop (to fuse loot), and dungeon entrance (to either enter from the beginning or visit floors that you&#39;ve been to).
  - **oo**** Randomly-generated maze**
    - Generic underground dungeon/maze. Randomly generated using a similar algorithm used in the GoDot project. Includes randomly generated paths that can spawn chests or enemies. The end is a hole in the floor that takes you to the next floor or a shrine that takes you back to Jacinto.
  - **oo**** Knight floor**
    - Small gladiator-themed dungeon arena found on every 5th floor. Starts you in a safe space, but going forward locks the door behind you and initiates the Knight fight. Defeating the knight unlocks powerful loot and the hole in the floor to the next floor or a shrine that takes you back to Jacinto.
  - **oo**** Balrog floor**
    - Large hellish-themed dungeon arena found on the 100th floor. Starts you in a safe space, but going forward locks the door behind you and initiates the Balrog fight. Defeating the Balrog unlocks overpowered equipment and using the shrine rolls credits and takes you to Jacinto.

###

-
### Goals

  - **oo**** Win Condition**
    - Small victories: Reaching the end of the floor with newfound loot. From there, you can either return to Jacinto or continue to the next floor.
    - Big Victory: Defeating the Balrog is the end of the game. You&#39;ll have a weapon that one-shots all enemies and armor that makes you invincible as a reward.
  - **oo**** Lose Condition**
    - Dying causes you to drop all of your loot on that floor and respawns you at Jacinto. It is then stored in a chest on that floor and visiting the Jacinto shrine puts a skull icon next to the floor where you died.
    - You must revisit the floor and complete it in order to get your loot back. However, if you die before reaching the end of that floor, you will lose that loot forever.
    - This is why it&#39;s important to either store spare loot in the Jacinto Bank and/or fuse loot to create more powerful equipment. You may also want to revisit older floors for a chance at getting more powerful loot.
-
### Actors

  - **oo**** The Player Character**
    - You play as a Revenant, a warrior that is technically immortal, but only because they respawn after dying.
    - You will have an inventory system that allows you to store, view, and equip weapons and armor. You can store up to 50 items, but the chest stores up to 500 items.
    - I am using a modular character pack, so other weapon types are possible, but I may have to stick with just swords and shields for the length of this class. Will definitely add more later.
  - **oo****  NPCs**
    - Achilles, the innkeeper
    - Geth, the banker
    - Auriel, the blacksmith
  - **oo**** Enemies**
    - **Note:** All enemies have heavily telegraphed attacks and hitboxes are attached to attacking weapons/limbs. This is to encourage the player to dodge, block, and counterattack tactically.
    - **Lizard Mob**
      - Generic lizard monster. Approaches player on sight and initiates basic melee attacks once within range.
    - **Knight**
      - Large knight boss. Approaches player upon entering boss room and initiates heavy telegraphed melee attacks one within range.
    - **Balrog**
      - Large(est) balrog boss. Approaches player upon entering boss room and initiates heavy telegraphed melee attacks and occasional fire attacks.